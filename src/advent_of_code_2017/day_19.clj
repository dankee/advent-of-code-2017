(ns advent-of-code-2017.day-19
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn rd-get [routing-diagram [x y]]
  (get-in routing-diagram [(- y) x]) )

(def direction-vector {:left  [-1  0]
                       :right [ 1  0]
                       :up    [ 0  1]
                       :down  [ 0 -1] })

(def potential-turns {:left  [:up   :down ]
                      :right [:up   :down ]
                      :up    [:left :right]
                      :down  [:left :right] })

(defn get-new-direction [routing-diagram [x y] direction]
  (->> direction
       potential-turns
       (filter #(-> %
                    direction-vector
                    ((fn [[delta-x delta-y]]
                       (if-let [potential-next (rd-get routing-diagram
                                                       [(+ x delta-x) (+ y delta-y)] )]
                         (not= potential-next \space) ) )) ))
       first ) )

(defn move [[x y] direction]
  (let [[delta-x delta-y] (direction-vector direction)]
    [(+ x delta-x) (+ y delta-y)] ) )

(defn part-1-and-2 []
  (let [routing-diagram (-> "resources/day-19-input.txt"
                            slurp
                            str/split-lines
                            (->>
                             (map vec)
                             vec ) )
        starting-x (-> routing-diagram
                       first
                       (->> (keep-indexed #(if (= %2 \|) %))
                            first ) ) ]
    (->> (loop [coordinates [starting-x 0]
                direction :down
                letters-seen []
                n-steps 0 ]
           (case (rd-get routing-diagram coordinates)
             \space [letters-seen n-steps]

             (\| \-) (recur (move coordinates direction)
                            direction
                            letters-seen
                            (inc n-steps) )

             \+ (let [new-direction (get-new-direction routing-diagram coordinates direction)]
                  (recur (move coordinates new-direction)
                         new-direction
                         letters-seen
                         (inc n-steps) ) )

             (recur (move coordinates direction)
                    direction
                    (conj letters-seen (rd-get routing-diagram coordinates))
                    (inc n-steps) ) ) )
         ((fn [[letters-seen n-steps]]
            [(apply str letters-seen) n-steps] ) ) ) ) )

(defn -main [& args]
  (println (time (part-1-and-2))) )
