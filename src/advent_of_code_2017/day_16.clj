(ns advent-of-code-2017.day-16
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def initial-state (vec "abcdefghijklmnop"))
(def initial-test-state (vec "abcde"))

(defn find-index [s k]
  (-> (keep-indexed #(if (= %2 k) %) s)
    first ) )

(def moves {:spin {:parse (fn [s]
                            (if-let [match (re-matches #"s(\d+)" s)]
                              (let [n (-> match second Long/parseLong)]
                                {:move :spin :n n} ) ) )
                   :execute (fn [s {:keys [n]}]
                              (let [i (- (count s) n)]
                                (vec (concat (subvec s i) (subvec s 0 i)))) ) }

            :exchange {:parse (fn [s]
                                (if-let [match (re-matches #"x(\d+)/(\d+)" s)]
                                  (let [[a b] (->> match (drop 1) (map #(Long/parseLong %)))]
                                    {:move :exchange :a a :b b} ) ) )
                       :execute (fn [s {:keys [a b]}]
                                  (let [temp (s a)]
                                    (-> s
                                      (assoc a (s b))
                                      (assoc b temp) ) ) ) }

            :partner {:parse (fn [s]
                               (if-let [match (re-matches #"p(\w)/(\w)" s)]
                                 (let [[a b] (->> match (drop 1) (map first))]
                                   {:move :partner :a a :b b} ) ) )
                      :execute (fn [s {:keys [a b]}]
                                 (let [a-index (find-index s a)
                                       b-index (find-index s b)
                                       temp (s a-index)]
                                   (-> s
                                     (assoc a-index (s b-index))
                                     (assoc b-index temp) ) ) ) } })

(defn parse-move [s]
  (some #(% s) (map #(-> % second :parse) moves)) )

(defn execute-move [state {:keys [move] :as move-args}]
  ((get-in moves [move :execute]) state move-args) )

(defn part-1 []
  (-> "resources/day-16-input.txt"
    slurp
    str/trim
    (str/split #",")
    (->>
      (map parse-move)
      (reduce execute-move initial-state)
      (apply str) ) ) )

(defn part-2 []
  (let [move-sequence (-> "resources/day-16-input.txt"
                        slurp
                        str/trim
                        (str/split #",")
                        (->>
                          (map parse-move) ) )
        state-sequence (iterate #(reduce execute-move % move-sequence) initial-state)
        [cycle-start cycle-end] (->> state-sequence
                                  (map-indexed vector)
                                  (reduce (fn [seen [i s]]
                                            (if (seen s)
                                              (reduced [(seen s) i])
                                              (assoc seen s i) ) )
                                          {} ) )
        cycle-length (- cycle-end cycle-start)
        real-number-of-iterations-needed (+ (mod 1000000000 cycle-length) cycle-start) ]
    (->> state-sequence
      (drop real-number-of-iterations-needed)
      first
      (apply str) ) ) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
