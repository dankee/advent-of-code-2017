(ns advent-of-code-2017.day-13
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn load-firewall []
  (-> "resources/day-13-input.txt"
    slurp
    str/split-lines
    (->>
      (map (fn [s]
             (map #(Long/parseLong %) (re-seq #"\d+" s)) )) ) ) )

(defn layers-caught-in [delay firewall]
  (filter (fn [[depth range]]
              (zero? (mod (+ depth delay) (* (dec range) 2))) )
          firewall ) )

(defn part-1 []
  (->> (load-firewall)
    (layers-caught-in 0)
    (map #(apply * %))
    (reduce +) ) )

(defn dont-get-caught? [firewall delay]
  (->> firewall
    (layers-caught-in delay)
    empty? ) )

(defn part-2 []
  (let [firewall (load-firewall)]
    (->> (range)
      (filter #(dont-get-caught? firewall %))
      first ) ) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
