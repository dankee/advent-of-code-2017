(ns advent-of-code-2017.day-08
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def parse-op-str {"inc" +
                   "dec" -})

(def parse-cond-op-str {">"  >
                        "<"  <
                        ">=" >=
                        "<=" <=
                        "==" =
                        "!=" not= })

(defn parse-instruction [s]
  (let [[target-reg op-str val-str _ cond-reg cond-op-str cond-val-str] (re-seq #"\S+" s)]
    {:target-reg target-reg
     :op (parse-op-str op-str)
     :val (Long/parseLong val-str)
     :cond-reg cond-reg
     :cond-op (parse-cond-op-str cond-op-str)
     :cond-val (Long/parseLong cond-val-str) } ) )

(defn part-1-and-2 []
  (let [final-state (-> "resources/day-08-input.txt"
                      slurp
                      str/split-lines
                      (->>
                        (map parse-instruction)
                        (reduce (fn [regs {:keys [target-reg op val cond-reg cond-op cond-val]}]
                                  (if (cond-op (get regs cond-reg 0) cond-val)
                                    (let [new-val (op (get regs target-reg 0) val)]
                                      (-> regs
                                        (assoc target-reg new-val)
                                        (update :max-val max new-val) ) )
                                    regs ) )
                                {:max-val 0} ) ) )]
    [(-> final-state
       (dissoc :max-val)
       (->>
         (map second)
         (apply max) ) )
     (final-state :max-val) ] ) )

(defn -main [& args]
  (println (part-1-and-2)) )
