(ns advent-of-code-2017.day-23
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn number-string? [s]
  (re-matches #"-?\d+" s) )

(def initial-state {:pc 0
                    :mul-count 0
                    :t 0
                    "a" 0
                    "b" 0
                    "c" 0
                    "d" 0
                    "e" 0
                    "f" 0
                    "g" 0
                    "h" 0 })

(defn part-1 []
  (let [program (-> "resources/day-23-input.txt"
                  slurp
                  str/split-lines
                  (->>
                    (map (fn [s]
                           (let [[op arg-1 arg-2] (re-seq #"\S+" s)]
                             (case op
                               "set" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (assoc arg-1 n)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (assoc arg-1 (% arg-2))
                                          (update :pc inc) ) )
                               "sub" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (update arg-1 - n)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (update arg-1 - (% arg-2))
                                          (update :pc inc) ) )
                               "mul" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (update arg-1 * n)
                                            (update :mul-count inc)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (update arg-1 * (% arg-2))
                                          (update :mul-count inc)
                                          (update :pc inc) ) )
                               "jnz" (if (number-string? arg-1)
                                       (if (number-string? arg-2)
                                         (let [n (Long/parseLong arg-2)]
                                           #(update % :pc + n) )
                                         #(update % :pc + (% arg-2)) )
                                       (if (number-string? arg-2)
                                         (let [n (Long/parseLong arg-2)]
                                           #(if (zero? (% arg-1))
                                              (update % :pc inc)
                                              (update % :pc + n) ) )
                                         #(if (zero? (% arg-1))
                                            (update % :pc inc)
                                            (update % :pc + (% arg-2)) ) ) )
                               :error ) ) ))
                    vec) ) ]
    (-> (loop [state initial-state]
          (if (< -1 (state :pc) (count program))
            (recur (-> ((program (state :pc)) state)
                     (update :t inc) ))
            state ) )
      :mul-count ) ) )

(def part-2-initial-state (assoc initial-state "a" 1))

(defn print-state-json [state]
  (print "{\"t\":" (state :t) ",")
  (print "\"pc\":" (state :pc) ",")
  (print "\"a\":" (state "a") ",")
  (print "\"b\":" (state "b") ",")
  (print "\"c\":" (state "c") ",")
  (print "\"d\":" (state "d") ",")
  (print "\"e\":" (state "e") ",")
  (print "\"f\":" (state "f") ",")
  (print "\"g\":" (state "g") ",")
  (println "\"h\":" (state "h") "},") )

(defn print-state [state]
  (print "t:" (state :t) "")
  (print "pc:" (state :pc) "")
  (print "a:" (state "a") "")
  (print "b:" (state "b") "")
  (print "c:" (state "c") "")
  (print "d:" (state "d") "")
  (print "e:" (state "e") "")
  (print "f:" (state "f") "")
  (print "g:" (state "g") "")
  (println "h:" (state "h")) )

(defn part-2 []
  (let [program (-> "resources/day-23-input.txt"
                  slurp
                  str/split-lines
                  (->>
                    (map (fn [s]
                           (let [[op arg-1 arg-2] (re-seq #"\S+" s)]
                             (case op
                               "set" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (assoc arg-1 n)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (assoc arg-1 (% arg-2))
                                          (update :pc inc) ) )
                               "sub" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (update arg-1 - n)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (update arg-1 - (% arg-2))
                                          (update :pc inc) ) )
                               "mul" (if (number-string? arg-2)
                                       (let [n (Long/parseLong arg-2)]
                                         #(-> %
                                            (update arg-1 * n)
                                            (update :mul-count inc)
                                            (update :pc inc) ) )
                                       #(-> %
                                          (update arg-1 * (% arg-2))
                                          (update :mul-count inc)
                                          (update :pc inc) ) )
                               "jnz" (if (number-string? arg-1)
                                       (if (number-string? arg-2)
                                         (let [n (Long/parseLong arg-2)]
                                           #(update % :pc + n) )
                                         #(update % :pc + (% arg-2)) )
                                       (if (number-string? arg-2)
                                         (let [n (Long/parseLong arg-2)]
                                           #(if (zero? (% arg-1))
                                              (update % :pc inc)
                                              (update % :pc + n) ) )
                                         #(if (zero? (% arg-1))
                                            (update % :pc inc)
                                            (update % :pc + (% arg-2)) ) ) )
                               :error ) ) ))
                    vec) ) ]
    #_(println "[")
    (-> (loop [state part-2-initial-state]
          #_(when (zero? (mod (state :t) 10000000))
              (print (new java.util.Date) "| ")
              (print-state state) )
          (if (< -1 (state :pc) (count program))
            (if (and (= (state :pc) 19)
                     (not (zero? (state "g"))) )
              (recur (-> state
                       (assoc "g" 0)
                       (assoc "f" (if (zero? (mod (state "b") (state "d")))
                                    0
                                    (state "f") ))
                       (assoc "e" (state "b")) ))
              (recur (-> ((program (state :pc)) state)
                       (update :t inc) )))
            state ) )
      (get "h") )
    #_(println "]") ) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
