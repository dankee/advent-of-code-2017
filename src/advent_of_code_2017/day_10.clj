(ns advent-of-code-2017.day-10
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn reverse-subvector [v start len]
  (if (< len 2)
    v
    (let [end (mod (+ start (dec len)) (count v))
          temp (nth v start) ]
      (recur (-> v
               (assoc start (nth v end))
               (assoc end temp) )
             (mod (inc start) (count v))
             (- len 2) ) ) ) )

(def initial-state {:current-position 0
                    :skip-size 0
                    :v (vec (range 256)) })

(defn run-round [initial-state lengths]
  (reduce (fn [{:keys [current-position skip-size] :as state} length]
                (-> state
                  (update :current-position #(mod (+ % length skip-size) 256))
                  (update :skip-size inc)
                  (update :v reverse-subvector current-position length) ) )
          initial-state
          lengths ) )

(defn part-1 []
  (-> "resources/day-10-input.txt"
    slurp
    (->>
      (re-seq #"\d+")
      (map #(Long/parseLong %))
      (run-round initial-state)
      :v
      (take 2)
      (reduce *) ) ) )

(def standard-length-suffix [17 31 73 47 23])

(defn knot-hash [s]
  (let [lengths (-> s
                  (->>
                    (map int) )
                  (concat standard-length-suffix) )]
    (->> (iterate #(run-round % lengths) initial-state)
      (drop 64)
      first
      :v
      (partition 16)
      (map #(apply bit-xor %))
      (map #(format "%02x" %))
      (apply str) ) ) )

(defn part-2 []
  (knot-hash (-> "resources/day-10-input.txt"
                slurp
                str/trim )) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
