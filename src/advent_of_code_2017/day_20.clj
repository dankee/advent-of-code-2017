(ns advent-of-code-2017.day-20
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn part-1 []
  (-> "resources/day-20-input.txt"
    slurp
    str/split-lines
    (->>
      (map #(->> (re-seq #"-?\d+" %)
              (drop 6)
              (map (fn [n] (Math/abs (Long/parseLong n))))
              (reduce +) ))
      (map vector (range))
      (apply min-key second) )
    first ) )

(defn tick-particle [{:keys [p v a]}]
  (let [new-v (map + v a)
        new-p (map + p new-v) ]
    {:p new-p :v new-v :a a} ) )

(defn tick [particles]
  (map tick-particle particles) )

(defn prune [particles]
  (->> particles
    (group-by :p)
    (filter #(= (-> % second count) 1))
    (map #(-> % second first)) ) )

(defn part-2 []
  (->> (iterate #(-> % tick prune)
                (-> "resources/day-20-input.txt"
                  slurp
                  str/split-lines
                  (->>
                    (map #(->> (re-seq #"-?\d+" %)
                            (map (fn [n] (Long/parseLong n)))
                            (partition 3)
                            ((fn [[position velocity acceleration]]
                               {:p position :v velocity :a acceleration} )) )) ) ) )
    (drop 50)
    first
    count ) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
