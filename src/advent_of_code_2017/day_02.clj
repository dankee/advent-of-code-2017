(ns advent-of-code-2017.day-02
  (:require [clojure.string :as str])
  (:gen-class) )

(defn calculate-checksum [f]
  (-> "resources/day-02-input.txt"
    slurp
    str/split-lines
    (->>
     (reduce (fn [acc line]
               (let [numbers (map #(Long/parseLong %) (re-seq #"\d+" line))]
                 (+ acc (f numbers)) ) )
             0 ) ) ) )

(defn part-1 []
  (calculate-checksum #(- (apply max %) (apply min %))) )

(defn part-2 []
  (calculate-checksum #(let [n (count %)
                             v (vec %) ]
                         (some (fn [[i j]]
                                 (let [a (v i)
                                       b (v j) ]
                                   (if (zero? (mod a b))
                                     (/ a b) ) ) )
                               (for [i (range n)
                                     j (range n)
                                     :when (not= i j) ]
                                 [i j] ) ) ) ) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
