(ns advent-of-code-2017.day-06
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s] )
  (:gen-class) )

(defn redistribute [initial-seen return-fn update-seen-fn]
  (loop [banks (-> "resources/day-06-input.txt"
                 slurp
                 (->>
                  (re-seq #"\w+")
                  (map #(Long/parseLong %)) )
                 vec )
         seen initial-seen
         cycles 0 ]
    (if (seen banks)
      (return-fn seen banks cycles)
      (recur (let [{:keys [max-value max-index]} (reduce (fn [{:keys [max-value] :as current-max} [i v]]
                                                           (if (> v max-value)
                                                             {:max-value v :max-index i}
                                                             current-max ) )
                                                         {:max-value -1}
                                                         (map vector (range) banks) )]
               (loop [b (assoc banks max-index 0)
                      i (mod (inc max-index) (count banks))
                      r max-value ]
                 (if (> r 0)
                   (recur (update b i inc)
                          (mod (inc i) (count banks))
                          (dec r) )
                   b ) ) )
             (update-seen-fn seen banks cycles)
             (inc cycles) ) ) ) )

(defn part-1 []
  (redistribute #{}
                (fn [_ _ cycles]
                  cycles )
                (fn [seen banks _]
                  (conj seen banks) ) ) )

(defn part-2 []
  (redistribute {}
                (fn [seen banks cycles]
                  (- cycles (seen banks)) )
                (fn [seen banks cycles]
                  (assoc seen banks cycles) ) ) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
