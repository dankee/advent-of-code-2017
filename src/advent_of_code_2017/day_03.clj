(ns advent-of-code-2017.day-03
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s] )
  (:gen-class) )

(def puzzle-input 277678)

(s/def ::value number?)
(s/def ::x number?)
(s/def ::y number?)
(s/def ::square (s/keys :req [::value ::x ::y]))
(s/def ::squares-so-far (s/map-of (s/tuple ::x ::y) ::value))

(s/def ::edge-length number?)
(s/def ::count number?)
(s/def ::direction #{:right :up :left :down})
(s/def ::state (s/keys :req [::edge-length ::count ::direction]))

(def next-direction {:right :up
                     :up    :left
                     :left  :down
                     :down  :right })

(def direction->update-args {:right [::x inc]
                             :up    [::y inc]
                             :left  [::x dec]
                             :down  [::y dec] })

(defn move [square direction]
  (-> (apply update square (direction->update-args direction))
    (update ::value inc) ) )

(s/fdef move
  :args (s/cat :square ::square :direction ::direction)
  :ret ::square )

(def initial-square #::{:value 1 :x 0 :y 0})

(defn part-1 [target-value]
  (let [{:keys [::x ::y]}
        (loop [{:keys [::value] :as square} initial-square
               {:keys [::edge-length ::count ::direction] :as state} #::{:edge-length 1 :count 0 :direction :right} ]
          (cond (= value target-value) square

                (= count edge-length)
                (recur square
                       (-> state
                         (update ::edge-length (if (#{:up :down} direction) inc identity))
                         (assoc ::count 0)
                         (update ::direction next-direction) ) )

                :else
                (recur (move square direction)
                       (update state ::count inc) ) ) ) ]
    (+ (Math/abs x) (Math/abs y)) ) )

(defn add-square-so-far [squares-so-far {:keys [::x ::y ::value]}]
  (assoc squares-so-far [x y] value) )

(s/fdef add-square-so-far
  :args (s/cat :squares-so-far ::squares-so-far :square ::square)
  :ret ::squares-so-far )

(defn move-2 [square direction squares-so-far]
  (let [{:keys [::x ::y] :as new-square} (apply update square (direction->update-args direction))]
    (assoc new-square ::value (reduce + (for [x-index (range (dec x) (+ x 2))
                                              y-index (range (dec y) (+ y 2))
                                              :when (not (and (= x-index x) (= y-index y))) ]
                                          (get squares-so-far [x-index y-index] 0) ))) ) )

(s/fdef move-2
  :args (s/cat :square ::square :direction ::direction :squares-so-far ::squares-so-far)
  :ret ::square )

(def initial-state #::{:edge-length 1
                       :count 0
                       :direction :right
                       :squares-so-far (add-square-so-far {} initial-square) })

(defn part-2 [target-value]
  (let [{:keys [::value]}
        (loop [{:keys [::value] :as square} initial-square
               {:keys [::edge-length ::count ::direction ::squares-so-far] :as state} initial-state ]
          (cond (>= value target-value) square

                (= count edge-length)
                (recur square
                       (-> state
                         (update ::edge-length (if (#{:up :down} direction) inc identity))
                         (assoc ::count 0)
                         (update ::direction next-direction) ) )

                :else
                (let [next-square (move-2 square direction squares-so-far)]
                  (recur next-square
                         (-> state
                           (update ::count inc)
                           (update ::squares-so-far add-square-so-far next-square) ) ) ) ) ) ]
    value ) )

(defn -main [& args]
  (println (part-1 puzzle-input))
  (println (part-2 puzzle-input)) )
