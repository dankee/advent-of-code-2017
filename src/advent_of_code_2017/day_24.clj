(ns advent-of-code-2017.day-24
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn parse-long [s]
  (Long/parseLong s) )

(defn load-graph []
  (-> "resources/day-24-input.txt"
    slurp
    str/split-lines
    (->>
      (map #(->> % (re-seq #"\d+") (map parse-long)))
      (reduce (fn [m [a b]]
                (-> m
                  (update a #(if (nil? %) {:neighbors #{b}} (update % :neighbors conj b)))
                  (update b #(if (nil? %) {:neighbors #{a}} (update % :neighbors conj a))) ) )
              {} ) ) ) )

(defn max-strength [g n visited-edges]
  (let [next-nodes (-> n
                     g
                     :neighbors
                     (->>
                       (filter #(not (visited-edges (sort [n %])))) ) )]
    (if (zero? (count next-nodes))
      0
      (-> next-nodes
        (->>
          (map #(+ (max-strength g % (conj visited-edges (sort [% n]))) n %))
          (apply max)) ) ) ) )

(defn part-1 []
  (max-strength (load-graph) 0 #{}) )

(defn max-depth [g n visited-edges]
  (let [next-nodes (-> n
                     g
                     :neighbors
                     (->>
                       (filter #(not (visited-edges (sort [n %])))) ) )]
    (if (zero? (count next-nodes))
      {:depth 0 :strength 0}
      (-> next-nodes
        (->>
          (map #(let [child-max (max-depth g % (conj visited-edges (sort [% n])))]
                  (-> child-max
                    (update :strength + % n)
                    (update :depth inc) ) ))
          (apply max-key #(+ (* (:depth %) 1000000) (:strength %))) ) ) ) ) )

(defn part-2 []
  (max-depth (load-graph) 0 #{}) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
