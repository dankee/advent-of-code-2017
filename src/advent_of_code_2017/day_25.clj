(ns advent-of-code-2017.day-25
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def blueprint-parser
  (insta/parser
   "<blueprint> = start-state n-steps instructions-for-state+
    start-state = <'Begin in state '> state-name <'.\n'>
    n-steps = <'Perform a diagnostic checksum after '> n <' steps.\n'>
    <n> = #'\\d+'
    instructions-for-state = <'\n'> in-state value-branch+
    value-branch = value-condition write-op move-dir next-state
    value-condition = <'  If the current value is '> n <':\n'>
    write-op = <'    - Write the value '> n <'.\n'>
    move-dir = <'    - Move one slot to the '> dir <'.\n'>
    <dir> = 'left' | 'right'
    next-state = <'    - Continue with state '> state-name <'.\n'>
    in-state = <'In state '> state-name <':\n'>
    <state-name> = #'\\w'" ) )

(defn parse-blueprints [s]
  (let [parser-output (insta/parse blueprint-parser s)
        o (-> parser-output
            (->>
              (take 2)
              (into {}) ) ) ]
    (-> parser-output
      (->>
        (drop 2)
        (map (fn [[_
                   [_ in-state]
                   [_ [_ value-a] & branch-a]
                   [_ [_ value-b] & branch-b] ]]
               [in-state
                {value-a (into {} branch-a)
                 value-b (into {} branch-b) }] ))
        (into o) ) ) ) )

(defn part-1 []
  (let [blueprints (-> "resources/day-25-input.txt"
                     slurp
                     parse-blueprints )]
    (loop [state (:start-state blueprints)
           steps-remaining (Long/parseLong (:n-steps blueprints))
           tape {}
           position 0 ]
      (if (zero? steps-remaining)
        (-> tape
          vals
          (->>
            (reduce +) ) )
        (let [current-value (get tape position 0)
              {:keys [write-op move-dir next-state]} (get-in blueprints [state (str current-value)]) ]
          (recur next-state
                 (dec steps-remaining)
                 (assoc tape position (Long/parseLong write-op))
                 ((if (= move-dir "left") dec inc) position) ) ) ) ) ) )

(defn -main [& args]
  (println (time (part-1))) )
