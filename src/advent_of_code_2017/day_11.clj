(ns advent-of-code-2017.day-11
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def direction->vector {"n"  [ 0  1]
                        "ne" [ 1  0]
                        "se" [ 1 -1]
                        "s"  [ 0 -1]
                        "sw" [-1  0]
                        "nw" [-1  1] })

(def initial-state {:current [0 0]
                    :max-distance 0 })

(defn hex-distance [[x y]]
  (if (< (* x y) 0)
    (max (Math/abs x) (Math/abs y))
    (+ (Math/abs x) (Math/abs y)) ) )

(defn part-1-and-2 []
  (-> "resources/day-11-input.txt"
    slurp
    (->>
      (re-seq #"\w+")
      (map direction->vector)
      (reduce (fn [{:keys [max-distance] [current-x current-y] :current} [x2 y2]]
                (let [next [(+ current-x x2) (+ current-y y2)]
                      next-distance (hex-distance next) ]
                  {:max-distance (max next-distance max-distance)
                   :current next } ) )
              initial-state ) )
    ((fn [{:keys [current max-distance]}]
       [(hex-distance current)  max-distance] )) ) )

(defn -main [& args]
  (println (part-1-and-2)) )
