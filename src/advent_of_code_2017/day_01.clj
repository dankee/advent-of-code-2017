(ns advent-of-code-2017.day-01
  (:require [clojure.string :as str])
  (:gen-class) )

(defn part-1 []
  (let [i (-> "resources/day-01-input.txt" slurp str/trim)
        n (count i) ]
    (->> i
      cycle
      (take (+ n 1))
      (partition 2 1)
      (filter (fn [[a b]] (= a b)))
      (map #(Long/parseLong (str (first %))))
      (reduce +) ) ) )

(defn part-2 []
  (let [i (-> "resources/day-01-input.txt" slurp str/trim)
        n (count i)
        j (->> i
            cycle
            (drop (/ n 2)) ) ]
    (->> (map vector i j)
      (filter (fn [[a b]] (= a b)))
      (map #(Long/parseLong (str (first %))))
      (reduce +) ) ) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
