(ns advent-of-code-2017.day-15
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def puzzle-input-a 591)
(def puzzle-input-b 393)

(def factor-a 16807)
(def factor-b 48271)
(def divisor 2147483647)
(def mask 0xFFFF)

(defn generator [factor seed]
  (iterate #(mod (* % factor) divisor) seed) )

(defn judge [generator-a generator-b n]
  (->> (map vector generator-a generator-b)
    (drop 1)
    (take n)
    (filter (fn [[a b]]
              (= (bit-and a mask) (bit-and b mask)) ))
    count ) )

(defn part-1 []
  (judge (generator factor-a puzzle-input-a)
         (generator factor-b puzzle-input-b)
         40000000 ) )

(def multiple-of-a 4)
(def multiple-of-b 8)

(defn picky-generator [factor seed multiple-of]
  (->> (iterate #(mod (* % factor) divisor) seed)
    (filter #(zero? (mod % multiple-of))) ) )

(defn part-2 []
  (judge (picky-generator factor-a puzzle-input-a multiple-of-a)
         (picky-generator factor-b puzzle-input-b multiple-of-b)
         5000000 ) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
