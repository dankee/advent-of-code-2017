(ns advent-of-code-2017.day-14
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta]
            [advent-of-code-2017.day-10 :as d10 ])
  (:gen-class) )

(def puzzle-input "stpzcrnm")

(defn row-binary-strings [s]
  (map #(-> %
          str
          (Byte/parseByte 16)
          (Integer/toString 2) )
       s ) )

(defn row-used-count [s]
  (->> (row-binary-strings s)
    (map #(->> % (re-seq #"1") count))
    (reduce +) ) )

(defn part-1 [prefix]
  (->> (range 128)
    (map #(str prefix "-" %))
    (map d10/knot-hash)
    (map row-used-count)
    (reduce +) ) )

(defn row-vector [s]
  (->> (row-binary-strings s)
    (map #(-> (format "%4s" %)))
    (apply str)
    vec ) )

(def initial-not-seen (set (for [x (range 128) y (range 128)] [x y])))

(defn used? [disk coordinates]
  (= (get-in disk coordinates) \1) )

(defn neighbors [[x y]]
  [[(dec x)       y]
   [(inc x)       y]
   [      x (dec y)]
   [      x (inc y)] ] )

(defn part-2 [prefix]
  (let [disk (->> (range 128)
               (map #(str prefix "-" %))
               (map d10/knot-hash)
               (map row-vector)
               vec )]
    (loop [not-seen initial-not-seen
           potential-group-members #{}
           group-count 0 ]
      (cond (empty? not-seen)
            group-count

            (empty? potential-group-members)
            (let [current (first not-seen)]
              (if (used? disk current)
                (recur (disj not-seen current)
                       (into potential-group-members (neighbors current))
                       (inc group-count) )
                (recur (disj not-seen current)
                       potential-group-members
                       group-count ) ) )

            :else
            (let [current (first potential-group-members)]
              (cond (not (not-seen current))
                    (recur not-seen
                           (disj potential-group-members current)
                           group-count )

                    (used? disk current)
                    (recur (disj not-seen current)
                           (-> potential-group-members
                             (disj current)
                             (into (neighbors current)) )
                           group-count )

                    :else
                    (recur (disj not-seen current)
                           (disj potential-group-members current)
                           group-count ) ) ) ) ) ) )

(defn -main [& args]
  (println (time (part-1 puzzle-input)))
  (println (time (part-2 puzzle-input))) )
