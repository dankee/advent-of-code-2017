(ns advent-of-code-2017.day-07
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn parse-tower-info [s]
  (insta/parse (insta/parser "<program-and-disc-info> = program-info disc-info?
                              <program-info> = name <' ('> weight <')'>
                              <disc-info> = <' -> '> disc-name other-disc-name*
                              <other-disc-name> = <', '> disc-name
                              <name> = word
                              <weight> = word
                              <disc-name> = word
                              <word> = #'\\w+'" )
               s ) )

(defn all-info []
  (-> "resources/day-07-input.txt"
    slurp
    str/split-lines
    (->>
      (map parse-tower-info)
      (map (fn [[name weight-string & children]]
             {:name name
              :weight (Long/parseLong weight-string)
              :children children } )) ) ) )

(defn get-root [all-info]
  (let [all-programs (set (map :name all-info))]
    (-> (reduce #(disj % %2) all-programs (->> all-info
                                            (map :children)
                                            (apply concat) ))
      first ) ) )

(defn part-1 [all-info]
  (get-root all-info) )

(defn get-weight-or-unbalanced-node [t node-name]
  (let [{:keys [weight children]} (t node-name)
        child-weights-or-unbalanced-node (map #(hash-map :name %
                                                         :weight-or-unbalanced-node (get-weight-or-unbalanced-node t %) )
                                              children ) ]
    (if-let [unbalanced-node-vector (first (filter (comp not number?)
                                                   (map :weight-or-unbalanced-node child-weights-or-unbalanced-node) ))]
      unbalanced-node-vector
      (let [groups (group-by :weight-or-unbalanced-node child-weights-or-unbalanced-node)]
        (if (> (count groups) 1)
          (sort-by (comp count second) groups)
          (apply + weight (map :weight-or-unbalanced-node child-weights-or-unbalanced-node)) ) ) ) ) )

(defn part-2 [all-info]
  (let [t (into {} (map (fn [{:keys [name] :as node}] [name node]) all-info))
        [[weight-with-children [{:keys [name]}]] [target-weight _]] (get-weight-or-unbalanced-node t (get-root all-info)) ]
    (+ (-> name t :weight) target-weight (- weight-with-children)) ) )

(defn -main [& args]
  (let [all-info (all-info)]
    (println (part-1 all-info))
    (println (part-2 all-info)) ) )
