(ns advent-of-code-2017.day-04
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s] )
  (:gen-class) )

(defn valid-passphrase-count [key-fn]
  (-> "resources/day-04-input.txt"
    slurp
    str/split-lines
    (->>
      (filter #(reduce (fn [acc elem]
                         (let [keyed-elem (key-fn elem)]
                           (if (acc keyed-elem)
                             (reduced false)
                             (conj acc keyed-elem) ) ) )
                       #{}
                       (re-seq #"\w+" %) ))
      count ) ) )

(defn part-1 []
  (valid-passphrase-count identity) )

(defn part-2 []
  (valid-passphrase-count sort) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
