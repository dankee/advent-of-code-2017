(ns advent-of-code-2017.day-05
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s] )
  (:gen-class) )

(defn find-exit [update-fn]
  (loop [instructions (-> "resources/day-05-input.txt"
                          slurp
                          str/split-lines
                          (->>
                           (map #(Long/parseLong %)) )
                          vec )
         index 0
         steps 0 ]
    (if (or (< index 0) (>= index (count instructions)))
      steps
      (let [next-index (+ index (instructions index))]
        (recur (update instructions index update-fn)
               next-index
               (inc steps)) ) ) ) )

(defn part-1 []
  (find-exit inc) )

(defn part-2 []
  (find-exit #(if (>= % 3) (dec %) (inc %))) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
