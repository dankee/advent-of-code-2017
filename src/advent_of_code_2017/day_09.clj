(ns advent-of-code-2017.day-09
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(s/def ::mode #{:normal :in-garbage :in-canceled})

(def initial-state {:depth 0
                    :mode :normal
                    :sum 0
                    :garbage-count 0 })

(defn part-1-and-2 []
  (-> "resources/day-09-input.txt"
    slurp
    (->>
     (reduce (fn [{:keys [depth mode] :as state} c]
               (case mode
                 :in-canceled
                 (assoc state :mode :in-garbage)

                 :in-garbage
                 (case c
                   \> (assoc state :mode :normal)
                   \! (assoc state :mode :in-canceled)
                   (update state :garbage-count inc) )

                 (case c
                   \< (assoc state :mode :in-garbage)
                   \{ (update state :depth inc)
                   \} (-> state
                        (update :sum + depth)
                        (update :depth dec) )
                   state ) ) )
             initial-state ) )
    ((juxt :sum :garbage-count)) ) )

(defn -main [& args]
  (println (part-1-and-2)) )
