(ns advent-of-code-2017.day-17
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def puzzle-input 386)

(defn cl-new [v]
  (let [cl {:value v :next (atom nil)}]
    (reset! (:next cl) cl) ) )

(defn cl-next [cl]
  (-> cl :next deref) )

(defn cl-insert [cl v]
  (let [new-node {:value v :next (atom (cl-next cl))}]
    (reset! (:next cl) new-node)
    new-node ) )

(defn cl-step [cl n]
  (if (zero? n)
    cl
    (recur (cl-next cl) (dec n)) ) )

(defn spinlock [n-insertions n-steps]
  (->> (range n-insertions)
    (map inc)
    (reduce #(-> %
               (cl-step n-steps)
               (cl-insert %2) )
            (cl-new 0) ) ) )

(defn part-1 [n-steps]
  (-> (spinlock 2017 n-steps)
    cl-next
    :value ) )

(defn part-2 [n-steps]
  (-> (spinlock 50000000 n-steps)
    (#(if (zero? (:value %))
        %
        (recur (cl-next %)) ))
    cl-next
    :value ) )

(defn -main [& args]
  (println (time (part-1 puzzle-input)))
  (println (time (part-2 puzzle-input))) )
