(ns advent-of-code-2017.day-21
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def initial-pattern [[\. \# \.] [\. \. \#] [\# \# \#]])

(defn flip [p]
  (mapv #(-> % reverse vec) p) )

(defn rot-c [n [x y]]
  (if (= n 2)
    [(- 1 y)       x]
    [y       (- 2 x)]) )

(defn rot [p]
  (let [n (count p)]
    (reduce #(assoc-in % (rot-c n %2) (get-in p %2))
            p
            (for [x (range n) y (range n)] [x y]) ) ) )

(defn all-pattern-variations [p]
  (->> (iterate rot p)
    (take 4)
    (mapcat #(vector % (flip %)))
    set ) )

(defn break-by [p n]
  (let [size (count p)]
    (vec
     (for [y (range 0 size n)]
       (vec
        (for [x (range 0 size n)]
          (->> (range y (+ y n))
               (mapv #(subvec (p %) x (+ x n))) ) )) )) ) )

(defn break [p]
  (if (zero? (mod (count p) 2))
    (break-by p 2)
    (break-by p 3) ) )

(defn unbreak [bp]
  (->> bp
    (map #(apply map (comp vec concat) %))
    (apply concat)
    vec ) )

(defn enhance [rules pattern]
  (some rules (all-pattern-variations pattern)) )

(defn tick [rules pattern]
  (let [bp (break pattern)
        n (count bp) ]
    (-> (reduce #(update-in % %2 (partial enhance rules))
                bp
                (for [x (range n)
                      y (range n) ]
                  [x y] ) )
        unbreak ) ) )

(defn fractal [n]
  (let [rules (-> "resources/day-21-input.txt"
                  slurp
                  str/split-lines
                  (->>
                    (map #(let [parts (re-seq #"[#.]+" %)]
                            (if (= (count parts) 5)
                              [(vec (map vec (take 2 parts)))
                               (vec (map vec (drop 2 parts))) ]
                              [(vec (map vec (take 3 parts)))
                               (vec (map vec (drop 3 parts))) ] ) ))
                    (into {}) ) )]
    (->> (iterate (partial tick rules) initial-pattern)
      (drop n)
      first
      flatten
      (filter #{\#})
      count ) ) )

(defn part-1 []
  (fractal 5) )

(defn part-2 []
  (fractal 18) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
