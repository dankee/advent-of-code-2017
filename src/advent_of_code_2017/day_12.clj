(ns advent-of-code-2017.day-12
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn load-g []
  (-> "resources/day-12-input.txt"
    slurp
    str/split-lines
    (->>
      (reduce #(let [[node & adjacents] (re-seq #"\d+" %2)]
                 (assoc % node adjacents) )
              {} ) ) ) )

(defn find-group [g n]
  (loop [seen #{}
         to-see [n] ]
    (if-let [current (first to-see)]
      (if (seen current)
        (recur seen
               (rest to-see) )
        (recur (conj seen current)
               (into (rest to-see) (g current)) ) )
      seen ) ) )

(defn part-1 []
  (-> (load-g)
    (find-group "0")
    count ) )

(def initial-state {:all-seen #{}
                    :group-count 0 })

(defn part-2 []
  (let [g (load-g)]
    (-> g
      keys
      (->>
       (reduce (fn [{:keys [all-seen] :as state} n]
                 (if (all-seen n)
                   state
                   (-> state
                     (update :all-seen into (find-group g n))
                     (update :group-count inc) ) ) )
               initial-state ) )
      :group-count ) ) )

(defn -main [& args]
  (println (part-1))
  (println (part-2)) )
