(ns advent-of-code-2017.day-18
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(defn parse-arg [s]
  (if (re-matches #"-?\d+" s)
    {:type :number :value (Long/parseLong s)}
    {:type :name :value (keyword s)} ) )

(defn resolve-arg [regs {:keys [type value]}]
  (case type
    :number value
    :name (get regs value 0) ) )

(def instructions {:send {:parse (fn [s]
                                         (if-let [m (re-matches #"snd (\S+)" s)]
                                           {:name :send
                                            :from (parse-arg (second m)) } ) )
                                :execute (fn [{:keys [from]} {:keys [regs] :as state}]
                                           (-> state
                                               (update :pc inc)
                                               (assoc :last-freq (resolve-arg regs from)) ) ) }

                   :set {:parse (fn [s]
                                  (if-let [m (re-matches #"set (\S+) (\S+)" s)]
                                    (let [[to from] (map parse-arg (rest m))]
                                      {:name :set
                                       :from from
                                       :to to } ) ) )
                         :execute (fn [{:keys [from to]} {:keys [regs] :as state}]
                                    (-> state
                                        (update :pc inc)
                                        (assoc-in [:regs (:value to)] (resolve-arg regs from)) ) ) }

                   :increase {:parse (fn [s]
                                       (if-let [m (re-matches #"add (\S+) (\S+)" s)]
                                         (let [[to amount] (map parse-arg (rest m))]
                                           {:name :increase
                                            :to to
                                            :amount amount } ) ) )
                              :execute (fn [{:keys [to amount]} {:keys [regs] :as state}]
                                         (-> state
                                             (update :pc inc)
                                             (assoc-in [:regs (:value to)] (+ (resolve-arg regs to) (resolve-arg regs amount))) ) ) }

                   :multiply {:parse (fn [s]
                                       (if-let [m (re-matches #"mul (\S+) (\S+)" s)]
                                         (let [[to amount] (map parse-arg (rest m))]
                                           {:name :multiply
                                            :to to
                                            :amount amount } ) ) )
                              :execute (fn [{:keys [to amount]} {:keys [regs] :as state}]
                                         (-> state
                                             (update :pc inc)
                                             (assoc-in [:regs (:value to)] (* (resolve-arg regs to) (resolve-arg regs amount))) ) ) }

                   :modulo {:parse (fn [s]
                                     (if-let [m (re-matches #"mod (\S+) (\S+)" s)]
                                       (let [[to amount] (map parse-arg (rest m))]
                                         {:name :modulo
                                          :to to
                                          :amount amount } ) ) )
                            :execute (fn [{:keys [to amount]} {:keys [regs] :as state}]
                                       (-> state
                                           (update :pc inc)
                                           (assoc-in [:regs (:value to)] (mod (resolve-arg regs to) (resolve-arg regs amount))) ) ) }

                   :receive {:parse (fn [s]
                                      (if-let [m (re-matches #"rcv (\S+)" s)]
                                        {:name :receive
                                         :cond (parse-arg (second m)) } ) )
                             :execute (fn [{:keys [cond]} {:keys [last-freq regs] :as state}]
                                        (if (zero? (resolve-arg regs cond))
                                          (update state :pc inc)
                                          (-> state
                                              (update :pc inc)
                                              (assoc :received last-freq) ) ) ) }

                   :maybe-jump {:parse (fn [s]
                                         (if-let [m (re-matches #"jgz (\S+) (\S+)" s)]
                                           (let [[cond amount] (map parse-arg (rest m))]
                                             {:name :maybe-jump
                                              :cond cond
                                              :amount amount } ) ) )
                                :execute (fn [{:keys [cond amount]} {:keys [regs] :as state}]
                                           (if (> (resolve-arg regs cond) 0)
                                             (update state :pc + (resolve-arg regs amount))
                                             (update state :pc inc) ) ) } })

(defn parse-instruction [s]
  (some #(% s) (->> instructions vals (map :parse))) )

(defn part-1 []
  (let [program (-> "resources/day-18-input.txt"
                    slurp
                    str/split-lines
                    (->>
                     (map parse-instruction)
                     vec ) )]
    (loop [{:keys [pc received] :as state} {:pc 0
                                             :received nil
                                             :regs {}
                                             :last-freq nil }]
      (or received
          (let [instruction (program pc)]
            (recur ((get-in instructions [(instruction :name) :execute]) instruction state)) ) ) ) ) )

(def part-2-instructions
  (-> instructions
      (assoc-in [:send :execute] (fn [{:keys [from]} {:keys [regs] :as state}]
                                   (-> state
                                       (update :pc inc)
                                       (update :out-q conj (resolve-arg regs from))
                                       (update :send-value-count #(if (nil? %) % (inc %))) ) ))
      (assoc-in [:receive :execute] (fn [{:keys [cond]} {:keys [in-buffer] :as state}]
                                      (-> state
                                          (update :pc inc)
                                          (assoc-in [:regs (:value cond)] in-buffer) ) ))) )

(defn parse-part-2-instruction [s]
  (some #(% s) (->> part-2-instructions vals (map :parse))) )

(defn part-2 []
  (-> (let [program (-> "resources/day-18-input.txt"
                        slurp
                        str/split-lines
                        (->>
                         (map parse-part-2-instruction)
                         vec ) )]
        (loop [p0-state {:pc 0
                         :regs {:p 0}
                         :out-q (clojure.lang.PersistentQueue/EMPTY)
                         :in-buffer nil }
               p1-state {:pc 0
                         :regs {:p 1}
                         :out-q (clojure.lang.PersistentQueue/EMPTY)
                         :in-buffer nil
                         :send-value-count 0 } ]
          (let [p0-instruction (program (:pc p0-state))
                p1-instruction (program (:pc p1-state)) ]
            (cond (and p0-instruction (not= (:name p0-instruction) :receive))
                  (recur ((get-in part-2-instructions [(:name p0-instruction) :execute]) p0-instruction p0-state)
                         p1-state )

                  (and p0-instruction (not (empty? (:out-q p1-state))))
                  (recur ((get-in part-2-instructions [(:name p0-instruction) :execute])
                          p0-instruction
                          (-> p0-state
                              (assoc :in-buffer (peek (:out-q p1-state))) ) )
                         (-> p1-state
                             (update :out-q pop) ) )

                  (and p1-instruction (not= (:name p1-instruction) :receive))
                  (recur p0-state
                         ((get-in part-2-instructions [(:name p1-instruction) :execute]) p1-instruction p1-state) )

                  (and p1-instruction (not (empty? (:out-q p0-state))))
                  (recur (-> p0-state
                             (update :out-q pop) )
                         ((get-in part-2-instructions [(:name p1-instruction) :execute])
                          p1-instruction
                          (-> p1-state
                              (assoc :in-buffer (peek (:out-q p0-state))) ) ) )

                  :else p1-state ) ) ) )
      :send-value-count ) )

(defn -main [& args]
  (println (time (part-1)))
  (println (time (part-2))) )
