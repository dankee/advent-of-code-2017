(ns advent-of-code-2017.day-22
  (:require [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [instaparse.core :as insta] )
  (:gen-class) )

(def turn-right {:up    :right
                 :right :down
                 :down  :left
                 :left  :up })

(def turn-left {:up    :left
                :left  :down
                :down  :right
                :right  :up })

(def direction-vector {:up    [ 0 -1]
                       :right [ 1  0]
                       :down  [ 0  1]
                       :left  [-1  0] })

(defn move [[x y] direction]
  (let [[delta-x delta-y] (direction-vector direction)]
    [(+ x delta-x) (+ y delta-y)] ) )

(defn part-1 [n]
  (let [initial-grid (-> "resources/day-22-input.txt"
                         slurp
                         str/split-lines
                         (->>
                          (map-indexed vector)
                          (reduce (fn [m [j row]]
                                    (assoc m j (reduce (fn [m [i char]]
                                                         (assoc m i char) )
                                                       {}
                                                       (map-indexed vector row) )) )
                                  {} ) ) )
        center (-> initial-grid count (/ 2) int) ]
    (loop [[x y] [center center]
           direction :up
           grid initial-grid
           burst-count 0
           infection-count 0 ]
      (if (< burst-count n)
        (let [currently-infected? (= (get-in grid [y x]) \#)
              new-direction (if currently-infected?
                              (turn-right direction)
                              (turn-left direction) )]
          (recur (move [x y] new-direction)
                 new-direction
                 (update-in grid [y x] #(if (= % \#) \. \#))
                 (inc burst-count)
                 (if currently-infected?
                   infection-count
                   (inc infection-count) ) ) )
        infection-count ) ) ) )

(def next-state {:clean :weakened
                 :weakened :infected
                 :infected :flagged
                 :flagged :clean
                 nil :weakened })

(def reverse-direction {:up    :down
                        :right :left
                        :down  :up
                        :left  :right })

(defn part-2 [n]
  (let [initial-grid (-> "resources/day-22-input.txt"
                         slurp
                         str/split-lines
                         (->>
                          (map-indexed vector)
                          (reduce (fn [m [j row]]
                                    (assoc m j (reduce (fn [m [i char]]
                                                         (assoc m i (if (= char \#) :infected :clean)) )
                                                       {}
                                                       (map-indexed vector row) )) )
                                  {} ) ) )
        center (-> initial-grid count (/ 2) int) ]
    (loop [[x y] [center center]
           direction :up
           grid initial-grid
           burst-count 0
           infection-count 0 ]
      (if (< burst-count n)
        (let [current-state (get-in grid [y x] :clean)
              new-direction (case current-state
                              :clean (turn-left direction)
                              :infected (turn-right direction)
                              :flagged (reverse-direction direction)
                              direction ) ]
          (recur (move [x y] new-direction)
                 new-direction
                 (update-in grid [y x] next-state)
                 (inc burst-count)
                 (if (= current-state :weakened)
                   (inc infection-count)
                   infection-count ) ) )
        infection-count ) ) ) )

(defn -main [& args]
  (println (time (part-1 10000)))
  (println (time (part-2 10000000))) )
