(defproject advent-of-code-2017 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha16"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [digest "1.4.6"]
                 [instaparse "1.4.8"]
                 [com.taoensso/tufte "1.1.2"] ]
  :main advent-of-code-2017.day-25 )
