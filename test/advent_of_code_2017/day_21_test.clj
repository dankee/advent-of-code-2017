(ns advent-of-code-2017.day-21-test
  (:require [clojure.test :refer :all]
            [advent-of-code-2017.day-21 :refer :all]))

(deftest break-test
  (testing "it works for size 2"
    (is (= (break [[\1 \2]
                   [\3 \4] ])
           [[[[\1 \2]
              [\3 \4] ]]] )) )

  (testing "it works for size 3"
    (is (= (break [[\1 \2 \3]
                   [\4 \5 \6]
                   [\7 \8 \9] ])
           [[[[\1 \2 \3]
              [\4 \5 \6]
              [\7 \8 \9] ]]] )) )

  (testing "it works for size 4"
    (is (= (break [[\1 \2 \3 \4]
                   [\5 \6 \7 \8]
                   [\9 \a \b \c]
                   [\d \e \f \g] ])
           [[[[\1 \2]
              [\5 \6] ]
             [[\3 \4]
              [\7 \8] ] ]
            [[[\9 \a]
              [\d \e] ]
             [[\b \c]
              [\f \g] ] ] ] )) )

  (testing "it works for size 9"
    (is (= (break [[\0 \1 \2 \3 \4 \5 \6 \7 \8]
                   [\9 \a \b \c \d \e \f \g \h]
                   [\i \j \k \l \m \n \o \p \q]
                   [\r \s \t \u \v \w \x \y \z]
                   [\A \B \C \D \E \F \G \H \I]
                   [\J \K \L \M \N \O \P \Q \R]
                   [\S \T \U \V \W \X \Y \Z \!]
                   [\@ \# \$ \% \^ \& \* \( \)]
                   [\` \~ \< \> \? \/ \, \. \-] ])
           [[[[\0 \1 \2]
              [\9 \a \b]
              [\i \j \k] ]
             [[\3 \4 \5]
              [\c \d \e]
              [\l \m \n] ]
             [[\6 \7 \8]
              [\f \g \h]
              [\o \p \q] ] ]
            [[[\r \s \t]
              [\A \B \C]
              [\J \K \L] ]
             [[\u \v \w]
              [\D \E \F]
              [\M \N \O] ]
             [[\x \y \z]
              [\G \H \I]
              [\P \Q \R] ] ]
            [[[\S \T \U]
              [\@ \# \$]
              [\` \~ \<] ]
             [[\V \W \X]
              [\% \^ \&]
              [\> \? \/] ]
             [[\Y \Z \!]
              [\* \( \)]
              [\, \. \-] ] ] ] )) ) )

(deftest unbreak-test
  (testing "it works"
    (is (= (unbreak [[[[\0 \1 \2]
                       [\9 \a \b]
                       [\i \j \k] ]
                      [[\3 \4 \5]
                       [\c \d \e]
                       [\l \m \n] ]
                      [[\6 \7 \8]
                       [\f \g \h]
                       [\o \p \q] ] ]
                     [[[\r \s \t]
                       [\A \B \C]
                       [\J \K \L] ]
                      [[\u \v \w]
                       [\D \E \F]
                       [\M \N \O] ]
                      [[\x \y \z]
                       [\G \H \I]
                       [\P \Q \R] ] ]
                     [[[\S \T \U]
                       [\@ \# \$]
                       [\` \~ \<] ]
                      [[\V \W \X]
                       [\% \^ \&]
                       [\> \? \/] ]
                      [[\Y \Z \!]
                       [\* \( \)]
                       [\, \. \-] ] ] ])
           [[\0 \1 \2 \3 \4 \5 \6 \7 \8]
            [\9 \a \b \c \d \e \f \g \h]
            [\i \j \k \l \m \n \o \p \q]
            [\r \s \t \u \v \w \x \y \z]
            [\A \B \C \D \E \F \G \H \I]
            [\J \K \L \M \N \O \P \Q \R]
            [\S \T \U \V \W \X \Y \Z \!]
            [\@ \# \$ \% \^ \& \* \( \)]
            [\` \~ \< \> \? \/ \, \. \-] ] )) ) )
