(ns advent-of-code-2017.day-10-test
  (:require [clojure.test :refer :all]
            [advent-of-code-2017.day-10 :refer :all]))

(deftest reverse-subvector-test
  (testing "reverses the subvector"
    (is (= (reverse-subvector [1 2 3 4 5] 1 3)
           [1 4 3 2 5] )) )

  (testing "handles wrapping"
    (is (= (reverse-subvector [1 2 3 4 5] 3 4)
           [5 4 3 2 1] )) )

  (testing "handles pivot point wrapping"
    (is (= (reverse-subvector [1 2 3 4 5] 4 4)
           [2 1 5 4 3] )) ) )
